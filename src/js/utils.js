export function string_to_slug(str) {
	str = str.trim().toLowerCase();

	// remove accents, swap ñ for n, etc
	var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
	var to   = "aaaaeeeeiiiioooouuuunc------";
	for (var i=0, l=from.length ; i<l ; i++) {
		str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}

	str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		.replace(/\s+/g, '-') // collapse whitespace and replace by -
		.replace(/-+/g, '-'); // collapse dashes

	return str;
}

export function getQueryVariable(variable) {
	let query = window.location.search.substring(1);
	
	let vars = query.split("&");
	for(let val of vars) {
		let pair = val.split("=");
		if(pair[0] == variable) {
			return decodeURIComponent(pair[1].replace(/\+/g, ' '));
		}
	}

	return;
}