/* 
 * Copyright (c) 2018 RuneScape Wiki team
 *
 * SPDX-License-Identifier: GPL-3.0
 */
import 'leaflet';
import 'leaflet-hash'; // Support for layers? https://github.com/KoGor/leaflet-fullHash
import 'leaflet-fullscreen';

import * as preferences from './preferences';

import './map/controls';
import './map/components';
import * as labels from './map/labels';
import * as locations from './map/locations';
import * as pathfind from './map/pathfind';

export const BASE_URL = 'http://167.99.150.82/';

export const MAP_BASE_ZOOM = 2;
export const MAP_MIN_ZOOM = -3;
export const MAP_MAX_ZOOM = 5;
export const MAP_MAX_NATIVE_ZOOM = 2;
export const MAP_CENTER = [ 3225, 3219 ]; // Lumbridge
export const MAP_BOUNDS = [ [0, 0], [12800, 12800] ];
export const MAP_TILE_URL = BASE_URL + 'maps/tiles/{z}_{p}_{x}_{-y}.png';
//export const MAP_TILE_URL = 'images/tiles/zoom-{z}/plane-{p}/{x}_{-y}.png';

export let _map;
export let _tileLayer;


export function bootstrap() {
	console.log("[MAP] init()");

	let domElem = document.getElementById('map');
	setup(domElem);
}

export function setup(domElem) {
	L.CRS.Simple.infinite = false;
	L.CRS.Simple.projection.bounds = new L.Bounds(MAP_BOUNDS);

	// Setup map
	_map = L.map(domElem, {
		crs: L.CRS.Simple,
		maxBounds: MAP_BOUNDS,
		maxBoundsViscosity: 0.5,
		zoomControl: false, // Replace default zoom controls with our own
		attributionControl: false, // Remove copyright, but reference Leaflet in the options
		fullscreenControl: true,
	});

	// Setup map events
	_map.on('load', () => {
		labels.init();
		pathfind.init();
	});

	// Setup controls
	setupControls();

	// Setup tiles
	setupTileLayer();

	let mapViewSet = false;

	// Check if request has highlighted bounds
	mapViewSet |= locations.highlight(domElem.dataset.highlight);
	
	// Initialize map at default coordinates and zoom level
	if(!mapViewSet) {
		// Restore location (if there is one available in the URL), or center map in Lumbridge.
		// This prevents the map from moving every time the page is loaded.
		let parsed = L.Hash.parseHash(location.hash);
		_map.setView(parsed.center || MAP_CENTER, parsed.zoom || MAP_BASE_ZOOM);
	}

	// Set location based on URL hash
	let hash = new L.Hash(_map);
}

function setupControls() {
	_map.fullscreenControl.setPosition('topright');

	L.control.customZoom().addTo(_map);
	L.control.help().addTo(_map);
	L.control.options().addTo(_map);

	L.control.plane().addTo(_map);
}

function setupTileLayer() {
	_tileLayer = L.mapTileLayer(MAP_TILE_URL, {
		bounds: MAP_BOUNDS,
		minZoom: MAP_MIN_ZOOM,
		maxZoom: MAP_MAX_ZOOM,
		maxNativeZoom: MAP_MAX_NATIVE_ZOOM,
		attribution: 'Map data &copy; <a href="http://runescape.wikia.com">RuneScape Wiki</a>',
	}).addTo(_map);

	// @ifdef DEBUG_TILES
	let tileDebug = new L.GridLayer();
	tileDebug.createTile = function(coords) {
		let tile = L.DomUtil.create('canvas', 'leaflet-tile');
		let ctx = tile.getContext('2d');
		let size = this.getTileSize();
		tile.width = size.x;
		tile.height = size.y;

		// calculate projection coordinates of top left tile pixel
		let nwPoint = coords.scaleBy(size);

		// calculate geographic coordinates of top left tile pixel
		let nw = _map.unproject(nwPoint, coords.z);

		ctx.fillStyle = 'white';
		ctx.fillRect(0, 0, size.x, 50);
		ctx.fillStyle = 'black';
		ctx.fillText('x: ' + coords.x + ', y: ' + coords.y + ', zoom: ' + coords.z, 20, 20);
		ctx.fillText('lat: ' + nw.lat + ', lon: ' + nw.lng, 20, 40);
		ctx.strokeStyle = 'red';
		ctx.beginPath();
		ctx.moveTo(0, 0);
		ctx.lineTo(size.x-1, 0);
		ctx.lineTo(size.x-1, size.y-1);
		ctx.lineTo(0, size.y-1);
		ctx.closePath();
		ctx.stroke();

		return tile;
	}
	tileDebug.addTo(_map);
	// @endif
}

L.Map.include({
	getBaseZoom: function() {
		return MAP_BASE_ZOOM;
	},

	getZoomPercentage: function(zoom = this.getBaseZoom()) {
		return 1 / this.getZoomScale(zoom);
	},
});

L.Control.include({
	_createButton(_this, html, title, className, container, fn = undefined) {
		let _createElement = function(element, html, title, className, container) {
			let elem = L.DomUtil.create(element, className, container);
			elem.innerHTML = html;
			elem.title = title;
		
			/*
			 * Will force screen readers like VoiceOver to read this as "Zoom in - button"
			 */
			elem.setAttribute('role', 'button');
			elem.setAttribute('aria-label', title);
		
			return elem;
		};

		let link = _createElement('a', html, title, className, container);
		link.href = '#';
	
		L.DomEvent.disableClickPropagation(link);
		L.DomEvent.on(link, 'click', L.DomEvent.stop);
		if(fn)
			L.DomEvent.on(link, 'click', fn, _this);
		L.DomEvent.on(link, 'click', _this._refocusOnMap, _this);
	
		return link;
	}
});