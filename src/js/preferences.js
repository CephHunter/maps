export function init() {
	console.log("[PREFERENCES] init()");

	if(!supported) {
		console.error("Storage is not supported!");
	}
}

let supported = isSupported();

export function get(id, _default = null) {
	if(!supported) {
		return null;
	}

	let val = localStorage.getItem(id);
	return val !== null ? val : _default;
}

export function set(id, value) {
	if(!supported) {
		return;
	}

	console.log("[PREFS] SET", id, value);
	localStorage.setItem(id, value);
}

export function remove(id) {
	if(!supported) {
		return;
	}

	localStorage.removeItem(id);
}

function isSupported() {
	try {
        localStorage.setItem('test', 'test');
        localStorage.removeItem('test');
        return true;
    } catch(e) {
        return false;
    }
}