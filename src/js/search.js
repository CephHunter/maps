import * as map from './map';
import * as locations from './map/locations';
import * as pathfind from './map/pathfind';


let searchContainer;
let search;
let searchResults;
let searchResultsEmpty;
let searchDetails;
let searchDetailsBack;
let searchDetailsDescription;
let searchDetailsButtonDirections;
let searchDetailsButtonWiki;
let searchCancelButton;

let directionsOverview;
let directionsOverviewLocationStart;
let directionsOverviewLocationEnd;
let directionsOverviewResults;
let directionsOverviewBack;
let directionsDetails;
let directionsDetailsName;
let directionsDetailsDuration;
let directionsDetailsSteps;
let directionsDetailsBack;

let currentResult;

const autocomplete = [
	{
		name: "Varrock",
		location: "Varrock",
		coords: { x: 1, y: 1 },
		type: "City",
		description: "<b>Varrock</b> is a city..."
	},
	{
		name: "Varrock Palace",
		location: "Varrock",
		coords: { x: 1, y: 1 },
		type: "Building",
		description: "<b>Varrock Palace</b> is the official residence of the king of Misthalin, as well as the nation's primary diplomatic and military centre. Since the Fourth Age, Varrock Palace has been a hub of economic..."
	},
	{
		name: "Valaine's Shop of Champions",
		location: "Varrock",
		coords: { x: 1, y: 1 },
		type: "Shop",
		description: "<b>Valaine's Shop</b> is... a shop..."
	},
	{
		name: "Vanescula Drakan",
		location: "Darkmeyer",
		coords: { x: 1, y: 1 },
		type: "NPC",
		description: "<b>Vanescula Drakan</b> is an NPC..."
	},
	{
		name: "Varrock Sewers",
		location: "Varrock",
		coords: { x: 1, y: 1 },
		type: "Dungeon",
		description: "<b>Varrock Sewers</b> is a dungeon..."
	},
	{
		name: "Grand Exchange",
		location: "Varrock",
		coords: { x: 1, y: 1 },
		type: "Shop",
		description: "<b>Grand Exchange</b> is great!"
	}
];

const routes = [
	{
		success: true,
		name: "spell/tablet",
		totalDuration: 8,
		requirements: [ "25 Magic", "Varrock Tasks" ],
		start:			{ x: 3233, y: 3219 },
		goal: 			{ x: 3164, y: 3488 },
		closestPoint:	{ x: 3164, y: 3488 },
		route: [
			{ title: "Lumbridge Castle", description: "", type: "start", coords: [{ x: 3233, y: 3219 }] },
			{ title: "Cast <a href='#'>GE Teleport</a>", description: "", type: "teleport", coords: [{ x: 3166, y: 3460 }] },
			{ title: "Walk", description: "", type: "walk", coords: [{ x: 3166, y: 3460 }, { x: 3164, y: 3488 }] },
			{ title: "Grand Exchange", description: "", type: "end", coords: [{ x: 3164, y: 3488 }] }
		],
		fastest: true
	},
	{
		success: true,
		name: "Lodestone Network",
		totalDuration: 27,
		requirements: [ "21 Agility" ],
		start:			{ x: 3233, y: 3219 },
		goal: 			{ x: 3164, y: 3488 },
		closestPoint:	{ x: 3164, y: 3488 },
		route: [
			{ title: "Lumbridge Castle", description: "", type: "start", coords: [{ x: 3233, y: 3219 }] },
			{ title: "Cast <a href='#'>Home Teleport</a> to the Edgeville Lodestone", description: "", type: "teleport", coords: [{ x: 3067, y: 3505 }] },
			{ title: "Walk", description: "", type: "walk", coords: [{ x: 3067, y: 3505 }, { x: 3114, y: 3508 }, { x: 3114, y: 3516 }, { x: 3139, y: 3517 }] },
			{ title: "Use the <a href='#'>Agility shortcut</a> under the wall (requires 21 Agility)", description: "", type: "agility", coords: [{ x: 3139, y: 3516 }] },
			{ title: "Walk", description: "", type: "walk", coords: [{ x: 3143, y: 3514 }, { x: 3164, y: 3488 }]  },
			{ title: "Grand Exchange", description: "", type: "end", coords: [{ x: 3164, y: 3488 }] }
		]
	},
	{
		success: true,
		name: "Lodestone Network",
		totalDuration: 44,
		requirements: [],
		start:			{ x: 3233, y: 3219 },
		goal: 			{ x: 3164, y: 3488 },
		closestPoint:	{ x: 3164, y: 3488 },
		route: [
			{ title: "Lumbridge Castle", description: "", type: "start", coords: [{ x: 3233, y: 3219 }] },
			{ title: "Cast <a href='#'>Home Teleport</a> to the Edgeville Lodestone", type: "teleport", coords: [{ x: 3067, y: 3505 }] },
			{ title: "Walk", description: "", type: "walk", coords: [{ x: 3067, y: 3505 }, { x: 3137, y: 3515 }, { x: 3135, y: 3463 }, { x: 3165, y: 3460 }, { x: 3164, y: 3488 }] },
			{ title: "Grand Exchange", description: "", type: "end", coords: [{ x: 3164, y: 3488 }] }
		]
		
	}
];

export function init() {
	console.log("[SEARCH] init()");

	searchContainer = document.getElementById('search');
	search = document.getElementById('search--input');
	searchResults = document.getElementById('search--results');
	searchResultsEmpty = document.getElementById('search--results-empty');
	searchDetails = document.getElementById('search--details');
	searchDetailsBack = document.getElementById('search--details__back');
	searchDetailsDescription = document.getElementById('search--details__description');
	searchDetailsButtonDirections = document.getElementById('search--details__button-directions');
	searchDetailsButtonWiki = document.getElementById('search--details__button-wiki');
	searchCancelButton = document.getElementById('search--cancel');

	search.addEventListener('keyup', e => _search((e.currentTarget || e.target).value));
	searchDetailsBack.addEventListener('click', e => {
		showSearchResults();

		search.value = search.dataset.value;
		search.dataset.value = '';
		search.focus();
	});

	searchDetailsButtonDirections.addEventListener('click', e => directions(currentResult));

	searchCancelButton.addEventListener('click', function() {
		console.log("Cancel button pressed");
		clear();
	});

	// TODO: find way to know when the user clicks out of the search container (to close it?)
	searchContainer.addEventListener('blur', e => {
		// Lose focus
		console.log("Lost focus.");
	});

	// Directions
	directionsOverview = document.getElementById('directions--overview');
	directionsOverviewLocationStart = document.getElementById('directions--overview__locations-start');
	directionsOverviewLocationEnd = document.getElementById('directions--overview__locations-end');
	directionsOverviewResults = document.getElementById('directions--overview__results');
	directionsOverviewBack = document.getElementById('directions--overview__back');
	directionsDetails = document.getElementById('directions--details');
	directionsDetailsName = document.getElementById('directions--details__name');
	directionsDetailsDuration = document.getElementById('directions--details__duration');
	directionsDetailsSteps = document.getElementById('directions--details__steps');
	directionsDetailsBack = document.getElementById('directions--details__back');

	directionsOverviewBack.addEventListener('click', e => showSearchDetails());
	directionsDetailsBack.addEventListener('click', e => showDirectionOverview());
}

export function clear() {
	search.value = '';

	searchResults.classList.add('hidden');
	searchResultsEmpty.classList.add('hidden');
	searchDetails.classList.add('hidden');
	directionsOverview.classList.add('hidden');
	directionsDetails.classList.add('hidden');

	pathfind.clear();
}

export function _search(val) {
	console.log("[SEARCH] Search:", val);

	let autocompleteResults = [];

	for(let term of autocomplete) {
		if(val.length > 0 && term.name.toLowerCase().startsWith(val.toLowerCase())) {
			autocompleteResults.push(term);
		}
	}

	show(val, autocompleteResults);
}

export function show(term, results) {
	console.log("[SEARCH] Showing:", results);

	showSearchResults();

	// Clear displayed search results
	while(searchResults.firstChild) {
		searchResults.removeChild(searchResults.firstChild);
	}

	// Show error if no results were found
	if(results.length > 0) {
		searchResults.classList.remove('hidden');
		searchResultsEmpty.classList.add('hidden');
	} else {
		searchResults.classList.add('hidden');
		searchResultsEmpty.classList.remove('hidden');
	}

	for(let result of results) {
		let item = document.createElement("li");

		item.classList.add('result');
		item.addEventListener('click', e => select(result));

		let create = (type, content) => {
			let elem = document.createElement("p");
			elem.classList.add(type);
			elem.innerHTML = content;
			return elem;
		};

		item.appendChild(create('title', result.name));
		item.appendChild(create('subtitle', result.type));

		searchResults.appendChild(item);
	};

	// TODO: find better way to clear location marker
	locations.highlight(undefined);
}

export function select(result) {
	console.log("[SEARCH] Details:", result);

	currentResult = result;

	showSearchDetails();

	// TODO: find better way to create location marker
	locations.highlight(result.name);

	search.dataset.value = search.value;
	search.value = result.name;

	searchDetailsDescription.innerHTML = result.description;
}

export function directions(result) {
	console.log("[SEARCH] Directions:", result);

	showDirectionOverview();

	let start = "Lumbridge Castle, Lumbridge";
	let end = result.name + ", " + result.location;

	// Update direction info
	directionsOverviewLocationStart.textContent = start;
	directionsOverviewLocationEnd.textContent = end;

	// Clear displayed routes
	while(directionsOverviewResults.firstChild) {
		directionsOverviewResults.removeChild(directionsOverviewResults.firstChild);
	}

	// Update routes
	let create = path => {
		let createParagraph = (type, content) => {
			let elem = document.createElement('p');
			elem.classList.add(...type);
			elem.textContent = content;
			return elem;
		}

		let root = document.createElement('li');
	
		root.appendChild(createParagraph(['duration'], path.totalDuration + ' secs'));
		root.appendChild(createParagraph(['method'], 'via ' + path.name));

		if(path.requirements !== undefined && path.requirements.length > 0) {
			// https://stackoverflow.com/a/34006337
			// Usage:
			//    [1,2,3,4].joinLast(', ', ' and ');
			Array.prototype.joinLast = function(all, last) {
				let arr = this.slice();                   // Make a copy so we don't mess with the original
				let lastItem = arr.splice(-1);            // Strip out the last element
				arr = arr.length ? [arr.join(all)] : [];  // Make an array with the non-last elements joined with our 'all' string, or make an empty array
				arr.push(lastItem);                       // Add last item back so we should have ["some string with first stuff split by 'all'", last item]; or we'll just have [lastItem] if there was only one item, or we'll have [] if there was nothing in the original array
				return arr.join(last);                    // Now we join the array with 'last'
			}

			root.appendChild(createParagraph(['requirements'], 'Requires ' + path.requirements.joinLast(', ', ' and ')));
		}

		root.appendChild(createParagraph(['details', 'next'], 'Details'));

		if(path.fastest) {
			root.classList.add('recommended');
		}

		// Add 'onclick' event
		root.addEventListener('click', e => showPath(result, path));

		return root;
	};

//	let routes = ...;

	for(let path of routes) {
		directionsOverviewResults.appendChild(create(path));

		for(let step of path.route) {
			if(step.coords) {
				if(step.coords.length > 1) {
					pathfind.drawLine(step.coords);
				} else {
					pathfind.drawMarker(step.coords[0].x, step.coords[0].y);
				}
			}
		}		
	}
}

export function showPath(result, path) {
	console.log("[SEARCH] Path:", result, path);

	showDirectionDetails();

	// Update route info
	directionsDetailsName.textContent = path.name;
	directionsDetailsDuration.textContent = '(' + path.totalDuration + ' sec)';

	// Clear displayed steps
	while(directionsDetailsSteps.firstChild) {
		directionsDetailsSteps.removeChild(directionsDetailsSteps.firstChild);
	}

	// Update steps
	let create = step => {
		let elem = document.createElement('li');
		elem.classList.add(step.type);
		elem.innerHTML = step.title;
		return elem;
	};

	for(let step of path.route) {
		directionsDetailsSteps.appendChild(create(step));

		if(step.coords) {
			if(step.coords.length > 1) {
				pathfind.drawLine(step.coords);
			} else {
				pathfind.drawMarker(step.coords[0].x, step.coords[0].y);
			}
		}
	}

	let startMarker = pathfind.drawMarker(path.start.x, path.start.y, { draggable: true });
	let endMarker = pathfind.drawMarker(path.closestPoint.x, path.closestPoint.y, { draggable: true });
}

function showSearchResults() {
	searchResults.classList.remove('hidden');
	searchDetails.classList.add('hidden');
	directionsOverview.classList.add('hidden');
	directionsDetails.classList.add('hidden');

	pathfind.clear();
}

function showSearchDetails() {
	searchResults.classList.add('hidden');
	searchDetails.classList.remove('hidden');
	directionsOverview.classList.add('hidden');
	directionsDetails.classList.add('hidden');

	pathfind.clear();
}

function showDirectionOverview() {
	searchResults.classList.add('hidden');
	searchDetails.classList.add('hidden');
	directionsOverview.classList.remove('hidden');
	directionsDetails.classList.add('hidden');

	pathfind.clear();
}

function showDirectionDetails() {
	searchResults.classList.add('hidden');
	searchDetails.classList.add('hidden');
	directionsOverview.classList.add('hidden');
	directionsDetails.classList.remove('hidden');

	pathfind.clear();
}