import { _map }  from '../map';

let _lookup = new Map();
let _mask;

// List of values are stored an array with [ y,x ] points
set('Blue Moon Inn', [ [ 3403,3218 ], [ 3403,3219 ], [ 3404,3219 ], [ 3404,3222 ], [ 3403,3222 ], [ 3403,3233 ], [ 3398,3233 ], [ 3397,3234 ], [ 3396,3234 ], [ 3395,3233 ], [ 3393,3233 ], [ 3393,3226 ], [ 3394,3225 ], [ 3394,3222 ], [ 3393,3221 ], [ 3393,3217 ], [ 3394,3216 ], [ 3397,3216 ], [ 3397,3218 ] ]);
set('Draynor Manor', [ [ 3374,3097 ], [ 3374,3097 ], [ 3374,3103 ], [ 3375,3104 ], [ 3375,3105 ], [ 3375,3105 ], [ 3374,3106 ], [ 3374,3111 ], [ 3375,3112 ], [ 3375,3113 ], [ 3374,3114 ], [ 3374,3120 ], [ 3361,3120 ], [ 3361,3127 ], [ 3354,3127 ], [ 3354,3127 ], [ 3354,3124 ], [ 3353,3123 ], [ 3353,3122 ], [ 3354,3121 ], [ 3354,3120 ], [ 3354,3120 ], [ 3353,3119 ], [ 3353,3118 ], [ 3354,3117 ], [ 3354,3100 ], [ 3353,3099 ], [ 3353,3098 ], [ 3354,3097 ], [ 3354,3096 ], [ 3353,3095 ], [ 3353,3094 ], [ 3354,3093 ], [ 3354,3091 ], [ 3364,3091 ], [ 3364,3097 ] ]);
set('Varrock Palace', [ [ 3501,3202 ], [ 3501,3205 ], [ 3499,3207 ], [ 3498,3207 ], [ 3498,3225 ], [ 3491,3225 ], [ 3491,3227 ], [ 3479,3227 ], [ 3479,3225 ], [ 3475,3225 ], [ 3474,3226 ], [ 3471,3226 ], [ 3470,3225 ], [ 3460,3225 ], [ 3460,3225 ], [ 3458,3223 ], [ 3458,3203 ], [ 3460,3201 ], [ 3470,3201 ], [ 3471,3200 ], [ 3474,3200 ], [ 3475,3201 ], [ 3475,3201 ], [ 3495,3201 ], [ 3496,3200 ], [ 3499,3200 ] ]);

export function get(name) {
	return _lookup.get(name);
}

export function set(name, points) {
	_lookup.set(name, points);
}

export function highlight(val) {
	if(_mask)
		_map.removeLayer(_mask);
	
	if(val == undefined)
		return false;

	let coords;
	if(_lookup.has(val)) {
		coords = _lookup.get(val);
	} else {
		// Attemp to parse value. Map string to array of coords.
		// Format: x,y/x,y/...
		coords = val.split('/').map(coord => coord.split(',').reverse());
	}

	let useOldStyle = false;
	if(useOldStyle) {
		_mask = L.polygon(coords, {
			weight: 5,
			color: '#041b7e',
			opacity: 0.9,
			fillColor: '#041b7e',
			fillOpacity: 0.5,
		}).addTo(_map);
	} else {
		_mask = L.mask(coords).addTo(_map);
	}

	// Create polygon with coordinates just to get corrent LatLngBounds
//	let bounds = new L.LatLngBounds(coords[0]);
	let bounds = L.polygon(coords).getBounds();
	if(bounds.isValid()) {
		_map.fitBounds(bounds, { padding: [50, 50] });

		// Reset hash to prevent the map from moving
		if(location.hash) {
			location.hash = '';
		}
		return true;
	} else {
		console.log("Invalid coordinates: ", coords);
		return false;
	}
}