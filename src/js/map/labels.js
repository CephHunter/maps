import { _map, _tileLayer }  from '../map';
import * as preferences from '../preferences';

let layerLabels;
let layerIcons;

export function init() {
	console.log("[MAP/LABELS] init()");

	layerLabels = L.layerGroup().addTo(_map);
	layerIcons = L.canvasLayer().addTo(_map);

	let baseMaps = {
		"Overworld": _tileLayer
	};

	let overlayMaps = {
		"Locations": layerLabels,
		"Icons": layerIcons,
	};

	L.control.layers(baseMaps, overlayMaps);//.addTo(_map);

	loadLabels();
	loadIcons();
}

function loadLabels() {
	fetch('data/labels.json')
		.then(response => {
			if(!response.ok) return []; // throw Error(response.statusText);
			return response.json();
		}).then(data => {
			for(let label of data) {
				// TODO: make these labels only appear at zoom level 3 or higher (>=200%)
				if(label.fontSize < 1) continue;

				let marker = L.textMarker([ label.y+0.5, label.x+0.5 ], label.text);
				layerLabels.addLayer(marker);
			}
		});
}

let _iconLayers = [];
let _iconLayersSub = [];

function loadIcons() {
	fetch('data/icons.json')
		.then(response => {
			if(!response.ok) return []; // throw Error(response.statusText);
			return response.json();
		}).then(data => {
			_buildIconLayers(data);
			_addVisLayers();
		});

	function _addVisLayers() {
		for(let layerName in _iconLayers) {
			let layer = _iconLayers[layerName];
			layerIcons.addLayer(layer);
		}
	}

	function _clearAllVisLayers() {
		for(let layerName in _iconLayers) {
			let layer = _iconLayers[layerName];
			layer.clearLayers();
		}
	}

	function _getCategoryIconLayer(category) {
		if(category in _iconLayers)
			return _iconLayers[category];

		let storageId = "category:" + name;
		let isVisible = true; // TODO: make some icons not visible by default

		let tmp;
		if(tmp = localStorage.getItem(storageId)) {
			isVisible = tmp === "true";
		}

		let layer = L.visibilityLayer({ visible: isVisible });
		_iconLayers[category] = layer;

		return layer;
	}

	function _getSubcategoryIconLayer(category, subcategory) {
		let storageId = "category:" + category + ":" + subcategory;

		if(storageId in _iconLayersSub)
			return _iconLayersSub[storageId];

		let baseLayer = _getCategoryIconLayer(category);
		let isVisible = baseLayer.getVisible();

		let tmp;
		if(tmp = localStorage.getItem(storageId)) {
			isVisible = tmp === "true";
		}

		let layer = L.visibilityLayer({ visible: isVisible });
		baseLayer.addLayer(layer);

		_iconLayersSub[storageId] = layer;
//		this._createSubtypeLootButton(category, subcategory);

/*		let self = this;
		$("#subloot_" + subcategory).change(function() {
			self._iconLayersSub[subcategory].setVisible(this.checked);
			localStorage.setItem(storageId, this.checked, {
				expires: self.options.cookieExpire
			});
		})
*/
		return layer;
	}

	function _buildIconLayers(data) {
		// Unwrap data
		let icons = data.icons;
		let coords = data.data;

		for(let label of coords) {
			let icon = icons[label.icon];
			let layer = _getSubcategoryIconLayer(icon.category || 'others', icon.subcategory || label.icon);

			let markerOptions = {
				icon: _getIcon('images/icons/' + label.icon + '.png', [ icon.width, icon.height ]),
				clickable: false,
				visible: false,
				plane: label.plane
			};

/*			if(this.options.individualScaleFactor.hasOwnProperty(marker.subcategory)) {
				markerOptions.scaleFactor = this.options.individualScaleFactor[marker.subcategory];
			}
*/
			let marker = new L.Marker([ label.y, label.x ], markerOptions);
			layer.addLayer(marker);
		}
	}

	function _getIcon(url, size, offset = [0,0]) {
		return L.divIcon({
			iconUrl: url,
			iconSize: size,
			spriteOffset: offset
		})
	}
}