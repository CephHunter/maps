L.Control.CustomZoom = L.Control.Zoom.extend({
	options: {
		position: 'topright',

		zoomInText: '<i class="fa fa-plus"></i>',//'+', // The text set on the 'zoom in' button.
		zoomInTitle: 'Zoom in', // The title set on the 'zoom in' button.
		zoomOutText: '<i class="fa fa-minus"></i>',//'&#x2212;', // The text set on the 'zoom out' button.
		zoomOutTitle: 'Zoom out' // The title set on the 'zoom out' button.
	},

	onAdd: function(map) {
		let containerName = 'leaflet-control-zoom';
		let container = L.DomUtil.create('div', containerName + ' leaflet-bar');
		let options = this.options;

		this._zoomInButton  = L.Control.prototype._createButton(this, options.zoomInText, options.zoomInTitle, containerName + '-in', container, this._zoomIn);
		this._zoomLevel     = L.Control.prototype._createButton(this, '', 'Zoom level', containerName + '-level', container, this._resetZoom);
		this._zoomOutButton = L.Control.prototype._createButton(this, options.zoomOutText, options.zoomOutTitle, containerName + '-out', container, this._zoomOut);

		this._updateDisabled();
		map.on('zoomend zoomlevelschange', this._updateDisabled, this);

		return container;
	},

	_updateDisabled: function() {
		L.Control.Zoom.prototype._updateDisabled.call(this);

		// Update displayed zoom level
		this._zoomLevel.textContent = (this._map.getZoomPercentage() * 100) + '%';
	},

	_resetZoom: function() {
		this._map.setZoom(this._map.getBaseZoom());
	}
});
L.control.customZoom = opts => new L.Control.CustomZoom(opts);