L.Control.Help = L.Control.extend({
	options: {
		position: 'topright',

		buttonText: '<i class="fa fa-question"></i>', // The text set on the 'options' button.		
		buttonTitle: 'Help', // The title set on the 'options' button.
	},

	onAdd: function(map) {
		let containerName = 'leaflet-control-help',
		container = L.DomUtil.create('div', containerName + ' leaflet-bar'),
		options = this.options;

		this._optionsButton = this._createButton(this, options.buttonText, options.buttonTitle, containerName + '-button', container);
		this._optionsButton.dataset.micromodalTrigger = 'modal-help';

		return container;
	},

	onRemove: function(map) {
		// Do nothing
	}
});
L.control.help = opts => new L.Control.Help(opts);