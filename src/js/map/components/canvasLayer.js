/**
 * Based on: https://izurvive.com
 */

import { _tileLayer } from '../../map'

L.CanvasLayer = L.GridLayer.extend({
	statics: {
		Images: {}
	},

	initialize: function(options) {
		L.setOptions(this, options);

		this._layerList = [];
		this._layerGrid = [];
		this._loadingImages = false;
		this._loadingFinishedCallbacks = [];

		this._plane = 0;
	},

	onAdd: function(map) {
		L.GridLayer.prototype.onAdd.call(this, map);

		this.options.maxNativeZoom = _tileLayer.options.maxNativeZoom;
		this.options.maxZoom = _tileLayer.options.maxZoom;

		this._currentZoom = this._clampZoom(map.getZoom());

        map.on('planechange', this._planeChange, this);
	},

	addLayer: function(layer) {
		if (!(layer instanceof L.VisibilityLayer)) {
			console.log("FIX: CanvasLayer.addLayer(layer) only accepts VisibilityLayer as argument");
			return this;
		}

		let id = L.Util.stamp(layer);
		this._layerList[id] = layer;
		layer._canvasLayer = this;
		
		for(let image of layer.getImages()) {
			this._loadImage(image);
		}
		this._addLayer(layer);

		L.GridLayer.prototype.redraw.call(this);

		return this;
	},

	createTile: function(coords, doneCallback) {
		let canvas = document.createElement("canvas");
		let canvasContext = canvas.getContext("2d");

		let size = this.getTileSize();
		canvas.width = size.x;
		canvas.height = size.y;
		
		let zoom = coords.z;
		if(this._currentZoom != zoom) {
			this._currentZoom = zoom;
			this._rebuildLayerGrid();
		}

		let layerGrid = this._layerGrid[coords.x] ? this._layerGrid[coords.x][coords.y] : [];
		if(layerGrid && layerGrid.length > 0) {
			// Only draw tile once all images have been loaded
			if(this._loadingImages > 0) {
				this._loadingFinishedCallbacks.push({
					method: this._drawTile,
					parameters: [coords, doneCallback, canvas, canvasContext, size, layerGrid]
				});
			} else {
				this._drawTile(coords, doneCallback, canvas, canvasContext, size, layerGrid);
			}
		}

		return canvas;
	},

	_drawTile: function(coords, doneCallback, canvas, canvasContext, tileSize, layerGrid) {
		// Filter icons from different planes
		layerGrid = layerGrid.filter(layer => layer.options.plane === this._plane);

		// Sort layers
		layerGrid.sort(function(a, b) {
			a = a._latlng.lat;
			b = b._latlng.lat;
			return a > b ? 1 : b > a ? -1 : 0
		});

		let scale = this._map.getZoomScale(this._map.getBaseZoom(), coords.z);

		let tilePos = L.point([ coords.x * tileSize.x, coords.y * tileSize.y ]);
		for(let layer of layerGrid) {
			if(!layer._visibilityLayer.getVisible()) {
				continue;
			}

			let iconOptions = layer.options.icon.options;
			let iconUrl = iconOptions.iconUrl;

			let icon = this._getImage(iconUrl);
			if (icon) {
				let size = L.point(iconOptions.iconSize);
				let offset = L.point(iconOptions.spriteOffset || [0,0]);
				let bounds = layer._bounds;
				let sizeScaled = layer.getSizeForScaling(coords.z);

				if(scale <= 1) {
					offset = ((size.y/2) * scale);
				} else {
					offset = (size.y/2);
				}

				// https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage
				//    drawImage(image, sourceX, sourceY, sourceWidth, sourceHeight, destinationX, destinationY, destinationWidth, destinationHeight)
				canvasContext.drawImage(
					icon,/*
					offset.x, // sprite
					offset.y, // sprite
					size.x,
					size.y,*/
					bounds.min.x - tilePos.x,
					bounds.min.y - tilePos.y - offset,
					sizeScaled.x,
					sizeScaled.y
				);
			} else {
				console.log("FIX: should not happen. Images should already be loaded. iconUrl: " + iconUrl);
			}
		}

		// TODO: Doesn't load new canvas without the 1ms timeout
		setTimeout(() => doneCallback(null, canvas), 1);
	},

	_fireLoadingCallbacks: function(self) {
		if(self._loadingImages) {
			return;
		}

		for(let callback of self._loadingFinishedCallbacks) {
			let args = Array.prototype.slice.call(callback.parameters, 0);
			// TODO: check if this is being called
			callback.method.apply(this, args);
		}

		self._loadingFinishedCallbacks = [];
	},

	_loadImage: function(iconUrl) {
		if (L.CanvasLayer.Images[iconUrl]) {
			return;
		}

		this._loadingImages++;
		
		let img = new Image;
		let self = this;

		img.onload = function() {
			this._loaded = true;
			self._loadingImages--;
			self._fireLoadingCallbacks(self);
		};
		img.onerror = function() {
			console.log("FIX: Unable to load image. imageUrl: " + iconUrl);
			L.CanvasLayer.Images[iconUrl] = false;
			self._loadingImages--;
			self._fireLoadingCallbacks(self);
		};

		img.src = iconUrl;
		L.CanvasLayer.Images[iconUrl] = img;
	},

	_getImage: function(iconUrl) {
		let img = L.CanvasLayer.Images[iconUrl];
		if(img) {
			if(img._loaded) {
				return img;
			} else {
				return false;
			}
		} else {
			console.log("FIX: should not happen? has the iconUrl changed? iconUrl: " + iconUrl);
			return false;
		}
	},

	_addLayer: function(layer) {
		let tileSize = this.options.tileSize;

		if(typeof layer.eachLayer === 'function') {
			layer.eachLayer(_layer => {
				if(_layer instanceof L.Marker) {
					this._addMarker(_layer, tileSize);
				} else if(_layer instanceof L.VisibilityLayer) {
					this._addLayer(_layer);
				} else {
					console.log("FIX: CanvasLayer only accepts Markers or VisibilityLayers.", _layer);
				}
			});
		}
	},

	_addMarker: function(layer, tileSize) {
		var pos = this._map.options.crs.latLngToPoint(layer.getLatLng(), this._currentZoom);
		let size = layer.getSizeForScaling(this._currentZoom);
		let center = L.point([size.x / 2, size.y / 2]);
		
		layer._bounds = L.bounds([
			[pos.x - center.x, pos.y - center.y],
			[pos.x + (size.x - center.x), pos.y + (size.y - center.y)]
		]);

		let boundsMin = L.point([Math.floor(layer._bounds.min.x / tileSize), Math.floor(layer._bounds.min.y / tileSize)]);
		let boundsMax = L.point([Math.floor(layer._bounds.max.x / tileSize), Math.floor(layer._bounds.max.y / tileSize)]);
		for (let y = boundsMin.y; y <= boundsMax.y; y++) {
			for (let x = boundsMin.x; x <= boundsMax.x; x++) {
				this._addMarkerToList(layer, L.point([x, y]));
			}
		}
	},

	_addMarkerToList: function(layer, coords) {
		if(!this._layerGrid[coords.x]) {
			this._layerGrid[coords.x] = {};
		}

		if(!this._layerGrid[coords.x][coords.y]) {
			this._layerGrid[coords.x][coords.y] = [];
		}

		this._layerGrid[coords.x][coords.y].push(layer);
	},

	_rebuildLayerGrid: function() {
		this._layerGrid = {};
		for(let id in this._layerList) {
			let layer = this._layerList[id];
			this._addLayer(layer);
		}
	},
	
    _planeChange: function(e) {
		this._plane = e.plane;
		this.redraw();
    },
});
L.canvasLayer = opts => new L.CanvasLayer(opts);

L.VisibilityLayer = L.LayerGroup.extend({
	options: {
		visible: true
	},

	initialize: function(options) {
		options = L.setOptions(this, options);
		L.LayerGroup.prototype.initialize.call(this, options);
	},

	// TODO: needed?
	onAdd: function(map) {
		L.LayerGroup.prototype.onAdd.call(this, map);
	},

	setVisible: function(newVisible, e) {
		let oldVisible = this.options.visible;
		this._setVisible(newVisible, e);

		if(oldVisible !== newVisible) {
			let canvasLayer = this._getCanvasLayer();
			if(canvasLayer !== null) {
				canvasLayer.redraw();
			}
		}
	},

	_setVisible: function(visible, e = undefined) {
		if(typeof e != "undefined") {
			this.eachLayer(function(layer) {
				if(layer instanceof L.VisibilityLayer) {
					layer._setVisible(visible, e);
				}
			})
		}

		this.options.visible = visible;
	},

	getVisible: function() {
		return this.options.visible;
	},

	getScaling: function() {
		return 1;
	},

	getImages: function() {
		let images = [];
		
		this.eachLayer(function(layer) {
			if (layer instanceof L.Marker) {
				images.push(layer.options.icon.options.iconUrl);
			} else if (layer instanceof L.VisibilityLayer) {
				// Merge arrays
				images.push.apply(images, layer.getImages());
			}
		})

		return images;
	},

	addLayer: function(layer) {
		if(layer instanceof L.Marker) {
			L.LayerGroup.prototype.addLayer.call(this, layer);
			layer._visibilityLayer = this;
		} else if(layer instanceof L.VisibilityLayer) {
			L.LayerGroup.prototype.addLayer.call(this, layer);
			layer._superLayer = this;
		} else {
			console.log("FIX: VisibilityLayer only accepts Markers or VisibilityLayers.", layer);
		}

		return this;
	},

	_getCanvasLayer: function() {
		let self = this;
		do {
			if(self._canvasLayer instanceof L.CanvasLayer)
				return self._canvasLayer;

			self = self._superLayer;
		} while (self instanceof L.VisibilityLayer);

		return null;
	}
});
L.visibilityLayer = opts => new L.VisibilityLayer(opts);

L.Marker = L.Marker.extend({
	getSizeForScaling: function(zoom) {
		let scale = 1;
		if(typeof this.options.scaleFactor != "undefined") {
			scale = this.options.scaleFactor;
		}

		let width = Math.floor(this.options.icon.options.iconSize[0] * scale);
		let height = Math.floor(this.options.icon.options.iconSize[1] * scale);
		return L.point(width, height);
	}
});