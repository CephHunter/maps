/**
 * Based on: https://github.com/eJuke/Leaflet.Canvas-Markers
 */

import { MAP_BASE_ZOOM } from '../../map';
import rbush from 'rbush';

L.CanvasLayer = L.Layer.extend({

	initialize: function(options) {
		L.setOptions(this, options);

		//_markers contains Points of markers currently displaying on map
		this._markers = new rbush;

		//_latlngMarkers contains Lat\Long coordinates of all markers in layer.
		this._latlngMarkers = new rbush;
		this._latlngMarkers.dirty = 0;
		this._latlngMarkers.total = 0;

		// Icon image cache
		this._imageLookup = new Map();
		
		// Add event listeners to initialized section.
		this._onClickListeners = [];
		this._onHoverListeners = [];
	},

	// Multiple layers at a time for rBush performance
	addMarkers: function(markers) {
		let tmpMark = [];
		let tmpLatLng = [];

		markers.forEach(marker => {
			if(marker.options.pane !== 'markerPane' || !marker.options.icon) {
				console.error('Layer isn\'t a marker');
				return;
			}

			let latlng = marker.getLatLng();
			let isDisplaying = this._map.getBounds().contains(latlng);
			let s = this._addMarker(marker, latlng, isDisplaying);

			//Only add to Point Lookup if we are on map
			if (isDisplaying) tmpMark.push(s[0]);

			tmpLatLng.push(s[1]);
		});

		this._markers.load(tmpMark);
		this._latlngMarkers.load(tmpLatLng);
	},

	// Adds single layer at a time. Less efficient for rBush
	addMarker: function(marker) {
		let latlng = marker.getLatLng();
		let isDisplaying = this._map.getBounds().contains(latlng);
		let dat = this._addMarker(marker, latlng, isDisplaying);

		// Only add to Point Lookup if we are on map
		if(isDisplaying) this._markers.insert(dat[0]);

		this._latlngMarkers.insert(dat[1]);
	},
/*
	addLayer: function(layer) {
		if(layer.options.pane !== 'markerPane' || !layer.options.icon) {
			console.error('Layer isn\'t a marker');
			return;
		}

		this.addMarker(layer);
	},

	addLayers: function(layers) {
		this.addMarkers(layers);
	},

	removeLayer: function(layer) {
		this.removeMarker(layer, true);
	},
*/
	removeMarker: function(marker, redraw = true) {
		// If we are removed point
		if(marker["minX"]) marker = marker.data;

		let latlng = marker.getLatLng();
		let isDisplaying = this._map.getBounds().contains(latlng);

		let markerData = {
			minX: latlng.lng,
			minY: latlng.lat,
			maxX: latlng.lng,
			maxY: latlng.lat,
			data: marker
		};

		this._latlngMarkers.remove(markerData, function(a,b) {
			return a.data._leaflet_id === b.data._leaflet_id;
		});

		this._latlngMarkers.total--;
		this._latlngMarkers.dirty++;

		if(isDisplaying && redraw) {
			this._redraw(true);
		}
	},

	onAdd: function(map) {
		this._zoomAnimated = map._zoomAnimated;

		if(!this._canvas)
			this._initCanvas();

		this.getPane().appendChild(this._canvas);

/*		map.on('zoomanim', e => {
			let elem = this._canvas;
			let prop = L.DomUtil.TRANSFORM;
			let transform = elem.style[prop];

			let scale = map.getZoomScale(e.zoom, 1) / 2;
			let translate = map.project(e.center, e.zoom);
//			let translate = map.getPixelOrigin().multiplyBy(scale).subtract(map._getNewPixelOrigin(e.center, e.zoom)).round();

//			console.log(translate, scale);
//			console.log("translate3d(" + translate.x + "px, " + translate.y + "px, 0px) scale(" + scale + ")");

			console.log("ZOOM | from:", map._zoom, ", to:", e.zoom);
			console.log("CENT | from:", map._center, ", to:", e.center);

			if (L.Browser.any3d) {
				L.DomUtil.setTransform(elem, translate, scale);
			} else {
				L.DomUtil.setPosition(elem, translate);
			}

			// workaround for case when transform is the same and so transitionend event is not fired
			if (transform === elem.style[prop] && this._animatingZoom) {
				this._onZoomTransitionEnd();
			}
		}, this);
*/	},

	getEvents: function () {
		var events = {
			moveend: this._reset,
			resize: this._reset,

			click: this._executeListeners,
			mousemove: this._executeListeners,
		};

		if (this._zoomAnimated) {
//			events.zoomanim = this._animateZoom;
		}

		return events;
	},

	onRemove: function(map) {
		this.getPane().removeChild(this._canvas);
	},

	clearLayers: function() {
		//this._markers = new rbush();
		//this._latlngMarkers = new rbush();
		this._markers.clear();
		this._latlngMarkers.clear();
		this._latlngMarkers.dirty = 0;
		this._latlngMarkers.total = 0;
		this._redraw(true);
	},

	_addMarker: function(marker, latlng, isDisplaying) {
		//Needed for pop-up & tooltip to work.
		marker._map = this._map;

		L.Util.stamp(marker);

		let pointPos = this._map.latLngToContainerPoint(latlng);
		let iconSize = marker.options.icon.options.iconSize;

		let adj_x = iconSize[0] / 2;
		let adj_y = iconSize[1] / 2;
		let ret = [
			{
				minX: (pointPos.x - adj_x),
				minY: (pointPos.y - adj_y),
				maxX: (pointPos.x + adj_x),
				maxY: (pointPos.y + adj_y),
				data: marker
			},{
				minX: latlng.lng,
				minY: latlng.lat,
				maxX: latlng.lng,
				maxY: latlng.lat,
				data: marker
			}
		];

		this._latlngMarkers.dirty++;
		this._latlngMarkers.total++;

		// Only draw if we are on map
		if(isDisplaying) this._drawMarker(marker, pointPos);

		return ret;
	},

	_drawMarker: function(marker, pointPos) {
		if(!pointPos)
			pointPos = this._map.latLngToContainerPoint(marker.getLatLng());

		let iconUrl = marker.options.icon.options.iconUrl;
		let draw = false;

		if(marker.canvas_img) {
			draw = true;
		} else {
			if(this._imageLookup.has(iconUrl)) {
				let imgLookup = this._imageLookup.get(iconUrl);
				if(imgLookup.complete) {
					if(imgLookup.naturalWidth > 0) {
						// Image is loaded, ready to draw
						marker.canvas_img = imgLookup;
						draw = true;
					} else {
						// Image is invalid.
						this._imageLookup.delete(iconUrl);
					}
				}
			} else {
				let img = new Image();
				img.src = iconUrl;
				
				// Store image in cache
				this._imageLookup.set(iconUrl, img);

				img.onload = () => {
					if(img.naturalWidth <= 0) {
						// Image is invalid.
						this._imageLookup.delete(iconUrl);
						return;
					}

					marker.canvas_img = img;
					this._drawImage(marker, pointPos);
				}
			}
		}

		if(draw) {
			this._drawImage(marker, pointPos);
		}
	},

	_drawImage: function(marker, pointPos) {
		let options = marker.options.icon.options;

		this._context.drawImage(
			marker.canvas_img,
			pointPos.x - options.iconAnchor[0] * this._scale,
			pointPos.y - options.iconAnchor[1] * this._scale,
			options.iconSize[0] * this._scale,
			options.iconSize[1] * this._scale
		);
	},

	_reset: function() {
		let topLeft = this._map.containerPointToLayerPoint([0, 0]);
		L.DomUtil.setPosition(this._canvas, topLeft);

		let size = this._map.getSize();

		this._canvas.width = size.x;
		this._canvas.height = size.y;

		this._redraw();
	},

	_redraw: function(clear = true) {
//		console.log("[_redraw]", clear);
		if (clear) this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);

		this._scale = this._map.getZoomScale(this._map.getZoom(), 2);

		let tmp = [];

		// If more than 10% of the markers are dirty (inserts\removals), reconstruct lookup for efficiency
		if (this._latlngMarkers.dirty / this._latlngMarkers.total >= 0.1) {
			this._latlngMarkers.all().forEach(function(e) {
				tmp.push(e);
			});

			this._latlngMarkers.clear();
			this._latlngMarkers.load(tmp);
			this._latlngMarkers.dirty = 0;
		}

		let mapBounds = this._map.getBounds();

		// Only re-draw what we are showing on the map.
		let mapBoxCoords = {
			minX: mapBounds.getWest(),
			minY: mapBounds.getSouth(),
			maxX: mapBounds.getEast(),
			maxY: mapBounds.getNorth(),
		};

		tmp = [];
		this._latlngMarkers.search(mapBoxCoords).forEach(e => {
			// Readjust Point Map
			let pointPos = this._map.latLngToContainerPoint(e.data.getLatLng());

			let iconSize = e.data.options.icon.options.iconSize;
			let adj_x = iconSize[0] / 2;
			let adj_y = iconSize[1] / 2;

			let newCoords = {
				minX: (pointPos.x - adj_x),
				minY: (pointPos.y - adj_y),
				maxX: (pointPos.x + adj_x),
				maxY: (pointPos.y + adj_y),
				data: e.data
			}

			tmp.push(newCoords);

			//Redraw points
			this._drawMarker(e.data, pointPos);
		});

		// Clear rBush & Bulk Load for performance
		this._markers.clear();
		this._markers.load(tmp);
	},

	_initCanvas: function() {
		this._canvas = L.DomUtil.create('canvas', 'leaflet-canvas-icon-layer leaflet-layer');
/*		let originProp = L.DomUtil.testProp([ 'transformOrigin', 'WebkitTransformOrigin', 'msTransformOrigin' ]);
		this._canvas.style[originProp] = '50% 50%';
*/
		let size = this._map.getSize();
		this._canvas.width = size.x;
		this._canvas.height = size.y;

		this._context = this._canvas.getContext('2d');

		this._context.mozImageSmoothingEnabled = false;
		this._context.webkitImageSmoothingEnabled = false;
		this._context.msImageSmoothingEnabled = false;
		this._context.imageSmoothingEnabled = false;
//		this._context.imageSmoothingQuality = "low"; // low, medium, high

		let animated = this._map.options.zoomAnimation && L.Browser.any3d;
		L.DomUtil.addClass(this._canvas, 'leaflet-zoom-' + (animated ? 'animated' : 'hide'));
	},

	addOnClickListener: function(listener) {
		this._onClickListeners.push(listener);
	},

	addOnHoverListener: function(listener) {
		this._onHoverListeners.push(listener);
	},

	_executeListeners: function(event) {
		let scaleX = this._scale * 10; // Assume icons are 10x20
		let scaleY = this._scale * 20; // Assume icons are 10x20

		let x = event.containerPoint.x;
		let y = event.containerPoint.y;
		let ret = this._markers.search({ minX: x - scaleX, minY: y, maxX: x + scaleX, maxY: y + scaleY });

		if(this._openToolTip) {
			this._openToolTip.closeTooltip();
			delete this._openToolTip;
		}	

		if (ret && ret.length > 0) {
			this._map._container.style.cursor = "pointer";

			if (event.type === "click") {
				let hasPopup = ret[0].data.getPopup();
				if(hasPopup) ret[0].data.openPopup();

				this._onClickListeners.forEach(function(listener) { listener(event, ret); });
			} else if(event.type === "mousemove") {
				let hasTooltip = ret[0].data.getTooltip();
				if(hasTooltip) {
					this._openToolTip = ret[0].data;
					ret[0].data.openTooltip();
				}

				this._onHoverListeners.forEach(function(listener) { listener(event, ret); });
			}
		} else {
			this._map._container.style.cursor = "";
		}
	}
});
L.canvasLayer = opts => new L.CanvasLayer(opts);