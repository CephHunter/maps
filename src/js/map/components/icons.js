import { MAP_BOUNDS }  from '../../map';


L.TextIcon = L.DivIcon.extend({
	options: {
		className: 'leaflet-div-icon marker-text-icon',
		iconSize: null,
	}
});
L.textIcon = (text, opts = {}) => new L.TextIcon(Object.assign({ html: text }, opts));

L.TextMarker = L.Marker.extend({
	initialize: function(latlng, options) {
		L.setOptions(this, options);
		this._latlng = L.latLng(latlng);
		this._scale = 1;
	},

	_initIcon: function() {
		L.Marker.prototype._initIcon.call(this);

		// Set up icon anchor dynamically, based on (now computed) icon bounds
		this._icon.iconAnchor = L.point([
			this._icon.offsetWidth / 2,
			this._icon.offsetHeight / 2
		]);

//		this._icon.popupAnchor = this._icon.iconAnchor;		
	},

	onAdd: function(map) {
		this._scale = map.getZoomPercentage();
		L.Marker.prototype.onAdd.call(this, map);
	},

	_animateZoom: function (e) {
		this._scale = this._map.getZoomScale(e.zoom, this._map.getBaseZoom());

		let pos = this._map._latLngToNewLayerPoint(this._latlng, e.zoom, e.center).round();
		this._setPos(pos);
	},

	_setPos: function(pos) {
		pos = pos.subtract(this._icon.iconAnchor.multiplyBy(this._scale));

		L.DomUtil.setTransform(this._icon, pos, this._scale);

		if (this._shadow) {
			L.DomUtil.setTransform(this._shadow, pos, this._scale);
		}

		this._zIndex = pos.y + this.options.zIndexOffset;
		this._resetZIndex();
	},
});
L.textMarker = (latlng, text) => new L.TextMarker(latlng, { icon: L.textIcon(text), alt: text });


// Based on: https://github.com/turban/Leaflet.Mask
L.Mask = L.Polygon.extend({
	options: {
		stroke: false,
		color: 'black',
		fillOpacity: 0.5,
		clickable: false
	},

	initialize: function(latLngs, options) {
		let outerBounds = new L.LatLngBounds(MAP_BOUNDS);
		let outerBoundsLatLngs = [
			outerBounds.getSouthWest(),
			outerBounds.getNorthWest(),
			outerBounds.getNorthEast(),
			outerBounds.getSouthEast()
		];

		L.Polygon.prototype.initialize.call(this, [ outerBoundsLatLngs, latLngs ], options);	
	},

});
L.mask = (latlng, options) => new L.Mask(latlng, options);