import { _map, _tileLayer, BASE_URL }  from '../map';
import * as utils from '../utils';
import * as preferences from '../preferences';
import * as search from '../search';

// Coords
export const COORDS_LUMBRIDGE = [ 3225, 3219 ];
export const COORDS_VARROCK   = [ 3425, 3211 ];

export let _layer;

export function init() {
	console.log("[MAP/PATHFIND] init()");

	_map.on('click', onMapClick);

	_layer = L.layerGroup().addTo(_map);
}

let _last = L.point(COORDS_LUMBRIDGE);

function onMapClick(e) {
	let point = L.point(Math.floor(e.latlng.lng), Math.floor(e.latlng.lat));

	console.log("Clicked:", point);

	clear();
	drawPath(_last, point);
	_last = point;
}

export function clear() {
	_layer.clearLayers();
}

export function drawPath(from, to) {
//	console.log("[PATHFINDER] from:", from, 'to:', to);
	let url = BASE_URL + 'pathfinder/path.json?from=' + from.x + ',' + from.y + '&to=' + to.x + ',' + to.y;

	let bypassCors;
	if(bypassCors = true) url = crossDomain(url);

	fetch(url)
		.then(function(response) {
			if(response.ok)
				return response.json();

			return {
				start: { x: from.x, y: from.y }, // Start
				closestPoint: { x: to.x, y: to.y }, // End
				route: [
					{
						coords: [
							{ x: from.x, y: from.y },
							{ x: to.x, y: to.y }
						]
					}
				]
			};
		}).then(function(data) {
			search.showPath(undefined, data);
/*			for(let route of data.route) {
				let points = [];
				for(let coord of route.coords) {
					points.push([ coord.y + 0.5, coord.x + 0.5 ]);
				}

				let color = route.id < 0 ? 'magenta': 'blue';
				let dash = route.id < 0 ? '1': '0.6,5';
				let line = L.polyline(points, { 'color': color, 'dashArray': dash });

				_layer.addLayer(line);
			}

			let startMarker = drawMarker(data.start.x, data.start.y, { draggable: true });
			let endMarker = drawMarker(data.closestPoint.x, data.closestPoint.y, { draggable: true });
*/		});

}

// Usage: drawMarker(...[x, y])
export function drawMarker(x, y, opts = {}) {
	// console.log("Creating marker at:",x,y);
	let marker = L.marker([ y + 0.5, x + 0.5 ], opts);
	_layer.addLayer(marker);
	return marker;
};

export function drawLine(points, color = 'blue', dash = '0.6,5') {
	let pointsFix = [];
	for(let coord of points) {
		pointsFix.push([ coord.y + 0.5, coord.x + 0.5 ]);
	}

//	let color = route.id < 0 ? 'magenta': 'blue';
//	let dash = route.id < 0 ? '1': '0.6,5';	
	let line = L.polyline(pointsFix, { 'color': color, 'dashArray': dash });
	_layer.addLayer(line);
}

function crossDomain(url, via = undefined) {
	switch (via) {
		case 'crossorigin':
			return 'http://crossorigin.me/' + url;
		case 'whateverorigin':
			return 'http://whateverorigin.org/get?url=' + encodeURIComponent(url) + '&callback=?';
		case 'anyorigin':
			return 'http://anyorigin.com/go/?url=' + encodeURIComponent(url) + '&callback=?';
		case 'corsanywhere':
		default:
			return 'https://cors-anywhere.herokuapp.com/' + url;
	}
}