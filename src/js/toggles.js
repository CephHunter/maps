import * as preferences from './preferences.js';

//NodeList.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];
//HTMLCollection.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];

export function init() {
	console.log("[TOGGLES] init()");
	
	let toggles = document.getElementsByClassName('toggle');
	for(let elem of toggles) {
		let id = elem.id;
		let checked = elem.checked;

		let pref = preferences.get(id);
		if(pref != null) {
			// Set value from the preferences
			checked = pref == 'true';
		} else {
			// Store default value into the preferences
			preferences.set(id, elem.checked);
		}

		elem.checked = checked;

		elem.addEventListener('change', e => {
			let elem = e.currentTarget || e.target;
			let id = elem.id;
			let checked = elem.checked;

			// Store new value into the preferences
			preferences.set(id, checked);
		});
	};
}