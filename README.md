# RuneScape Wiki Maps Project
We are creating a dynamic, interactive RuneScape map that can be embedded in wiki articles. It will support zooming, panning, markup (labels, markers, paths, icons), and links between dungeon maps. This map will also be available as a standalone application.

# Useful links
* [Project information](https://docs.google.com/document/d/1xv7iVBkG6eWDrXFhIYi77AVodJc0D-4Aqn0ncZXKJfw/edit#heading=h.kk1966kbedef)
* [Teleport data](https://docs.google.com/spreadsheets/d/16h4qJwfHFd7aBtrE4hEqRl1w23WOlQgsnijx5-_lBgs/edit#gid=0)
* [UI Mockups](https://sketch.cloud/s/Eq5O2)

# State of the art
The following technologies are being used in this project.

| Asset | Name | Links |
| --- | --- | --- |
| HTML | [Nunjucks](https://mozilla.github.io/nunjucks/) | [Docs](https://mozilla.github.io/nunjucks/templating.html) |
| CSS | [Sass](https://sass-lang.com/) |[Getting Started](https://sass-lang.com/guide) • [Docs](https://sass-lang.com/documentation/file.SASS_REFERENCE.html) |
| JS | [ES6+](http://ecmascript.org/)  | [What's New in ES6](http://exploringjs.com/es6/)  |

# Installation
To build the assets in this project you'll need [Node.js](https://nodejs.org/en/download/]).
## Initial setup
Run `npm install`. This will install all dependecies needed.

## Building assets
Every time you modify any asset (found in the `src/` directory), the assets will need to be rebuilt.
* To build the assets, execute `npx gulp`
*  All the assets will be avaiable in the `dist/` directory