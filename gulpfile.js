// Gulp.js configuration
/*jshint esversion: 6 */

/* Modules */
const gulp = require('gulp'),
	fs = require('fs'),
	notifier = require('node-notifier'),
	plumber = require('gulp-plumber-notifier'),
	source = require('vinyl-source-stream'),
	buffer = require('vinyl-buffer'),
	newer = require('gulp-newer'),
	rename = require('gulp-rename'),
	sourcemaps = require('gulp-sourcemaps'),
	preprocess = require("gulp-preprocess"),
	data = require('gulp-data'),
	gzip = require('gulp-gzip'),
	// Image
	imagemin = require('gulp-imagemin'),
	imageminMozjpeg = require('imagemin-mozjpeg'),
	pngquant = require('gulp-pngquant'),
	webp = require('gulp-webp'),
	svgo = require('gulp-svgo'),
	// HTML
	nunjucks = require('gulp-nunjucks-render'),
	beautify = require('gulp-html-beautify'),
	htmlclean = require('gulp-htmlclean'),
	// JS
	browserify = require('browserify'),
	babelify = require('babelify'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	// CSS
	sass = require('gulp-sass'),
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer'),
	mqpacker = require('css-mqpacker'),
	csso = require('gulp-csso');

// Check coverage at http://browserl.ist
// Current: 87.56% (last 2 versions or >1% market share)
const coverage = [ 'last 2 versions', '>1%' ];

// Use 'export NODE_ENV=production' on Linux, or 'set NODE_ENV=production' on Windows.
let devBuild = (process.env.NODE_ENV !== 'production');
//devBuild = false;

let preprocessOpts = {
	context: {
		DEBUG: true,
		//DEBUG_TILES: true,
	}
};

let dirs = {
	html: {
		src: 'src/html/',
		build: 'dist/',
		_nunjucks: {
			src: 'src/html/pages/',
			templates: 'src/html/templates/',
			data: './src/html/data/'
		}
	},
	img: {
		src: 'src/images/',
		build: 'dist/images/'
	},
	css: {
		src: 'src/scss/',
		build: 'dist/assets/css/'
	},
	js: {
		src: 'src/js/',
		build: 'dist/assets/js/'
	},
	data: {
		src: 'src/data/',
		build: 'dist/data/'
	},
	assets: {
		src: 'assets/',
		build: 'dist/assets/'
	}
};



/* Tasks */
// Default task
gulp.task('default', [ 'run', 'watch' ]);

// Run all tasks
gulp.task('run', [ 'images', 'gzip', 'html', 'css', 'js', 'assets' ]);

// Watch for changes
gulp.task('watch', function() {

//	gulp.watch(dirs.img.src + '**/*',  [ 'images' ]); // Image changes
	gulp.watch(dirs.data.src + '**/*',  [ 'gzip' ]); // JSON file changes
	gulp.watch(dirs.html.src + '**/*', [ 'html' ]); // HTML changes
	gulp.watch(dirs.js.src + '**/*',   [ 'js' ]); // javascript changes	
	gulp.watch(dirs.css.src + '**/*',  [ 'css' ]); // css changes

});


// Image processing
gulp.task('images', [ 'images-mozjpeg', 'images-png', 'images-webp', 'images-svg' ], function() {
	let out = dirs.img.build;
	
	return this;
});

gulp.task('images-mozjpeg', function() {
	let out = dirs.img.build;
	
	return gulp.src(dirs.img.src + '**/*.{jpg,jpeg}')
		.pipe(plumber())
		.pipe(newer(out))
		.pipe(imagemin([imageminMozjpeg({
			quality: 85
		})]))
		.pipe(gulp.dest(out));
});

gulp.task('images-png', function() {
	let out = dirs.img.build;
	
	return gulp.src(dirs.img.src + '**/*.png')
		.pipe(plumber())
		.pipe(newer(out))
		.pipe(imagemin({
			progressive: true,
			use: [pngquant()],
		}).on('error', e => {
			console.log("Error processing: " + e.fileName + "\n", e.message);
			this.emit('end');
		}))
		.pipe(gulp.dest(out));
});

gulp.task('images-webp', function() {
	let out = dirs.img.build;
	
	return gulp.src(dirs.img.src + '**/*.{jpg,jpeg}')
		.pipe(plumber())
		.pipe(newer(out))
		.pipe(webp({
			quality: 85,
			preset: 'photo',
			method: 6
		}))
		.pipe(gulp.dest(out));
});

gulp.task('images-svg', function() {
	let out = dirs.img.build;
	
	return gulp.src(dirs.img.src + '**/*.{svg}')
		.pipe(plumber())
		.pipe(newer(out))
		.pipe(svgo())
		.pipe(gulp.dest(out));
});

// JSON processing
gulp.task('gzip', function() {
	let out = dirs.data.build;

	let gzipOpts = {
		gzipOptions: {
			level: 9 // Best compression (default: 6)
		}
	}
	
	return gulp.src(dirs.data.src + '**/*')
		.pipe(plumber())
		.pipe(newer(out))
		.pipe(gulp.dest(out))
		.pipe(gzip(gzipOpts))
		.pipe(gulp.dest(out));
});

// HTML processing
gulp.task('html', function() {
	let out = dirs.html.build;

	let nunjucksOpts = {
		path: [ dirs.html._nunjucks.templates ]
	}

	let beautifyOpts = {
		indent_char: '	',
		indent_size: 1,
		preserve_newlines: false
	}

	return gulp.src(dirs.html._nunjucks.src + '**/*.{html,nunjucks,nj}')
		.pipe(plumber())
		.pipe(data(() => Object.assign({}, 
			require(dirs.html._nunjucks.data + 'tabs.json'),
			require(dirs.html._nunjucks.data + 'toggles.json')
		)))
		.pipe(preprocess(preprocessOpts))
		.pipe(nunjucks(nunjucksOpts))
		.pipe(beautify(beautifyOpts))
		.pipe(gulp.dest(out))
//		.pipe(htmlclean()) // Minify
//		.pipe(rename({ suffix: '.min' }))
//		.pipe(gulp.dest(out));
});

// JavaScript processing
gulp.task('js', function() {
	let babelOpts = {
		presets: [
			[ 'env', {
				targets: {
					browsers: coverage
				},
				useBuiltIns: 'entry'
			}]
		]
	};

	let browserifyOpts = {
		entries: [
			dirs.js.src + 'main.js'
		],
		debug: devBuild,
		transform: [
			babelify.configure(babelOpts)
		]
	};

	let handleError = function(err) {
		console.log(err.stack);
		notifier.notify({ 'title': 'Compile Error', 'message': err.message });
		this.emit('end'); // End this stream
	}

	// Set up the browserify instance on a task basis
	return browserify(browserifyOpts).bundle()
		.on('error', handleError)
		.pipe(source('app.js'))
		.pipe(buffer())
		.pipe(preprocess(preprocessOpts))
		.pipe(gulp.dest(dirs.js.build))
		.pipe(uglify()) // Minify
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest(dirs.js.build))
});

// CSS processing
gulp.task('css', function() {
	let sassOpts = {
		outputStyle: 'nested',
		imagePath: 'images/',
		precision: 5,
		errLogToConsole: true
	};

	let autoprefixerOpts = {
		browsers: coverage
	};

	let postCssOpts = [
		autoprefixer(autoprefixerOpts),
		mqpacker
	];

	let cssoOpts = {
		restructure: false,
		sourceMap: true
	};

	return gulp.src([ dirs.css.src + '*.scss' ])
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(sass(sassOpts).on('error', sass.logError))
		.pipe(postcss(postCssOpts))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(dirs.css.build))
		.pipe(csso(cssoOpts)) // Minify
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest(dirs.css.build));
});

// Copy assets to build folder
gulp.task('assets', function() {
	return gulp.src( [ dirs.assets.src + '**/*'])
		.pipe(gulp.dest(dirs.assets.build));
});