UI:
 * Proper responsive design...


Search:
 * Use arrow keys to navigate through search results.
 * Show "No results found. Try..."
 * Allow generic searches (like "weapon's shop near Lumbridge", or "NPCs from Varrock")
 * Mark regions (show the whole Lumbridge "province" when searching for Lumbridge)


Toggles:
 * Add buttons "All" and "None" to toggle th whole group.
 * Add wiki-links to locations


Tabs:
 * Disabled tabs.
 * Use arrow keys to navigate through tabs (when the dialog is first opened).


Map:
 * Labels/makers with clickable links
 * Serve simplified version through SVG


SEO:
 * Support card embed by FB, Twitter, OpenGraph, ...